'use strict';

angular
	.module('gitHack')
	.factory('profileProjectListFactory',function ($resource, $window, BASIC_URL) {
		return $resource(
			BASIC_URL+ '/projects/:id',
			{id: '@id'}
			)
	});