(function() {
  'use strict';

  angular
    .module('gitHack')
    .controller('profileController', profileController);

  /** @ngInject */
  function profileController($state, $window, $timeout, webDevTec, toastr, profileProjectListFactory, profileGroupListFactory, profileGroupMembersListFactory) {
    var vm = this;
    vm.timeLine= [];
    vm.projects= [];
    vm.groups= [];
    vm.groupMembers= [];

    vm.user= JSON.parse($window.sessionStorage.user);

    vm.listProjects= function () {
      vm.clearTimeLine();
      vm.projects= profileProjectListFactory.query();
    };

    vm.listGroups= function () {
      vm.clearTimeLine();
      vm.groups= profileGroupListFactory.query();
    }

    vm.listGroupMembers= function (index) {

      var group= vm.groups[index];
      vm.groupMembers[index]= profileGroupMembersListFactory.query({id: group.id});
    }

    vm.clearTimeLine= function () {
      vm.timeLine= [];
      vm.projects= [];
      vm.groups= [];
      vm.groupMembers= [];
    }

    vm.createProyect= function () {
      $state.go('createProject');
    }

    vm.createGroup= function () {
      $state.go('createGroup');
    }

    vm.deleteProject= function (index) {
      var project= vm.projects[index];
      profileProjectListFactory.delete({id: project.id})
    }

    vm.logOut= function () {
      $window.sessionStorage.removeItem('user');
      $state.go('home');
    }

  }
})();
