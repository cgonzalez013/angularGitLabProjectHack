'use strict';

angular
	.module('gitHack')
	.factory('profileGroupListFactory',function ($resource, $window, BASIC_URL) {
		return $resource(
			BASIC_URL+ '/groups'
			)
	})
	.factory('profileGroupMembersListFactory',function ($resource, $window, BASIC_URL) {
		return $resource(
			BASIC_URL+ '/groups/:id/members',
			{id: '@id'}
			)
	})
	;