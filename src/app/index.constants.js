/* global malarkey:false, moment:false */
(function() {
  'use strict';

  angular
    .module('gitHack')
    .constant('malarkey', malarkey)
    .constant('moment', moment)
    .constant('BASIC_URL', 'https://gitlab.com/api/v3');

})();
