(function() {
  'use strict';

  angular
    .module('gitHack')
    .config(routerConfig);

  /** @ngInject */
  function routerConfig($stateProvider, $urlRouterProvider) {
    $stateProvider
      .state('home', {
        url: '/',
        templateUrl: 'app/main/main.html',
        controller: 'MainController',
        controllerAs: 'main'
      })

      .state('profile', {
        url: '/profile',
        templateUrl: 'app/profile/profile.html',
        controller: 'profileController',
        controllerAs: 'profile'
      })

      .state('createProject', {
        url: '/createProject',
        templateUrl: 'app/createProject/createProject.html',
        controller: 'createProjectController',
        controllerAs: 'createProject'
      })

      .state('createGroup', {
        url: '/createGroup',
        templateUrl: 'app/createGroup/createGroup.html',
        controller: 'createGroupController',
        controllerAs: 'createGroup'
      });

    $urlRouterProvider.otherwise('/');
  }

})();
