'use strict';

angular
	.module('gitHack')
	.factory('createProjectFactory',function ($resource, $window, BASIC_URL) {
		return $resource(
			BASIC_URL+ '/projects'
			)
	})