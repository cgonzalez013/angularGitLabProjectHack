(function() {
  'use strict';

  angular
    .module('gitHack')
    .controller('createProjectController', createProjectController);

  /** @ngInject */
  function createProjectController($state, $window, $timeout, webDevTec, toastr, createProjectFactory) {
    var vm = this;
    vm.project= {};

    vm.backToProfile= function () {
      $state.go('profile');
    }

    vm.createProject= function () {
      createProjectFactory.save(vm.project);
      $state.go('profile');
    }

  }
})();