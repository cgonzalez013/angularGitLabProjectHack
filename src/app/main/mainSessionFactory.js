'use strict';

angular
	.module('gitHack')
	.factory('mainSessionFactory',function ($http,$window, BASIC_URL) {
		return {
			login:function (user, callback) {
				return $http({
					method:'POST',
					url: BASIC_URL+ '/session',
					data: user
				}).success( function (response) {
					$window.sessionStorage.user=JSON.stringify(response);
					callback();
				}).error( function (response) {
					console.log(response);
				});
			}

		};
	});