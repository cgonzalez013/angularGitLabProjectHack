(function() {
  'use strict';

  angular
    .module('gitHack')
    .controller('MainController', MainController);

  /** @ngInject */
  function MainController($state, $window, $timeout, webDevTec, toastr, mainSessionFactory) {
    var vm = this;

    vm.user= {};

    vm.login= function () {
      mainSessionFactory.login(vm.user, function () {
        $state.go('profile');
      });
    }
  }
})();
