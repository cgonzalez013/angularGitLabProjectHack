(function() {
  'use strict';

  angular
    .module('gitHack')
    .run(runBlock);

  /** @ngInject */
  function runBlock($log) {

    $log.debug('runBlock end');
  }

})();
