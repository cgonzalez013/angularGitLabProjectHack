(function() {
  'use strict';

  angular
    .module('gitHack', 
    	[
    	'ngAnimate', 
    	'ngTouch', 
    	'ngSanitize', 
    	'ngMessages', 
    	'ngAria', 
    	'ngResource', 
    	'ui.router', 
    	'ui.bootstrap', 
    	'toastr'
    	]);

})();
