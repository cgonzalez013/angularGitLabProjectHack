(function() {
  'use strict';

  angular
    .module('gitHack')
    .controller('createGroupController', createGroupController);

  /** @ngInject */
  function createGroupController($state, $window, $timeout, webDevTec, toastr, createGroupFactory) {
    var vm = this;
    vm.group= {};

    vm.backToProfile= function () {
      $state.go('profile');
    }

    vm.createGroup= function () {
      createGroupFactory.save(vm.group);
      $state.go('profile');
    }

  }
})();