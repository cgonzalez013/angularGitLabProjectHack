'use strict';

angular
	.module('gitHack')
	.factory('createGroupFactory',function ($resource, $window, BASIC_URL) {
		return $resource(
			BASIC_URL+ '/groups/:id',
			{id: '@id'}
			)
	})